# Azure Terraform OPA Example

This project is (will be) a working example demonstrating:

- Centralized, end-to-end CI/CD pipeline automation for Terraform code
- Working Terraform code to deploy a VM to Microsoft Azure
- Working OPA integration demonstrating policy-as-code and preventing changes to infrastructure that violate policy

 - [Workflow](#workflow)
 - [Getting Started](#getting-started)
 - [Planning](#planning)
 - [Applying](#applying)

## Workflow

## Getting Started

## Planning

## Applying


