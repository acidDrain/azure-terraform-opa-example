locals {
  resource-group-name = "${var.target.productname}-${replace(var.target.location, "/ /", "-")}"
  targetname          = var.target.productname
  location            = var.target.location
  baseTags = {
    Department  = "IT Security"
    Project     = "azure-terraform-opa-example"
    Environment = "test"
    Type        = "demo"
    Expiration  = "2021-07-15"
    FedRamp     = true
    Product     = var.target.productname
    Location    = var.target.location
  }
}

# Configure the Azure Provider
provider "azurerm" {
  # whilst the `version` attribute is optional, we recommend pinning to a given version of the Provider
  features {}
  client_id       = var.ARM_CLIENT_ID
  client_secret   = var.ARM_CLIENT_SECRET
  subscription_id = var.ARM_SUBSCRIPTION_ID
  tenant_id       = var.ARM_TENANT_ID
}

# Create a resource group
resource "azurerm_resource_group" "main" {
  name     = local.resource-group-name
  location = local.location
}

module "networks" {
  source              = "./modules/networks"
  location            = local.location
  resource-group-name = local.resource-group-name
  network_details     = var.network_details
}

resource "azurerm_linux_virtual_machine" "bastion-server" {
  name                = "bastion-server-linux"
  resource_group_name = local.resource-group-name
  location            = local.location
  size                = "Standard_B1ls"
  admin_username      = var.sysadmin
  network_interface_ids = [
    module.networks.bastion-nic-id
  ]

  admin_ssh_key {
    username   = var.sysadmin
    public_key = var.sysadmin-pub-rsa-key
  }

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "20.04-LTS"
    version   = "latest"
  }
}

resource "azurerm_managed_disk" "linux-storage-disk" {
  name                 = "${var.servername}-disk1"
  location             = local.location
  resource_group_name  = local.resource-group-name
  storage_account_type = "Standard_LRS"
  create_option        = "Empty"
  disk_size_gb         = 10
}

resource "azurerm_virtual_machine_data_disk_attachment" "bastion-disk" {
  managed_disk_id    = azurerm_managed_disk.linux-storage-disk.id
  virtual_machine_id = azurerm_linux_virtual_machine.bastion-server.id
  lun                = "10"
  caching            = "ReadWrite"
}
