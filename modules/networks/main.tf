# locals {
#   bastion = {
#     network_prefix = var.network_details.bastion_network_prefix
#     tags           = ["bastion"]
#   }
#   dmz = {
#     network_prefix = var.network_details.dmz_network_prefix
#     tags           = ["dmz", "public-inbound"]
#   }
#   restricted = {
#     network_prefix = var.network_details.restricted_network_prefix
#     tags           = ["restricted", "internal", "bastion-only"]
#   }

# }

resource "azurerm_subnet" "bastion" {
  resource_group_name  = var.resource-group-name
  virtual_network_name = azurerm_virtual_network.bastion.name
  name                 = "bastion"
  address_prefixes     = local.bastion.network_prefix
}

resource "azurerm_network_interface" "bastion-nic" {
  name                = "bastion-nic"
  location            = var.location
  resource_group_name = var.resource-group-name

  ip_configuration {
    name                          = "bastion-network-nic"
    subnet_id                     = azurerm_subnet.bastion.id
    private_ip_address_allocation = "Dynamic"
  }
}

resource "azurerm_subnet" "restricted" {
  resource_group_name  = var.resource-group-name
  virtual_network_name = azurerm_virtual_network.restricted.name
  name                 = "restricted"
  address_prefixes     = local.restricted.network_prefix
}

resource "azurerm_subnet" "dmz" {
  resource_group_name  = var.resource-group-name
  virtual_network_name = azurerm_virtual_network.dmz.name
  name                 = "dmz"
  address_prefixes     = local.dmz.network_prefix
}
