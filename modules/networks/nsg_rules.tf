resource "azurerm_network_security_rule" "allow-bastion" {
  name                        = "allow-bastion"
  priority                    = 100
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "*"
  source_port_range           = "*"
  destination_port_range      = "22"
  source_address_prefix       = azurerm_virtual_network.bastion.address_space[0]
  destination_address_prefix  = azurerm_virtual_network.restricted.address_space[0]
  resource_group_name         = var.resource-group-name
  network_security_group_name = azurerm_network_security_group.restricted.name
}

resource "azurerm_network_security_rule" "default-deny" {
  name                        = "default-deny"
  priority                    = 101
  direction                   = "Inbound"
  access                      = "Deny"
  protocol                    = "*"
  source_port_range           = "*"
  destination_port_range      = "*"
  source_address_prefix       = "*"
  destination_address_prefix  = azurerm_virtual_network.restricted.address_space[0]
  resource_group_name         = var.resource-group-name
  network_security_group_name = azurerm_network_security_group.restricted.name
}

resource "azurerm_network_security_rule" "dmz-tcp-80" {
  name                        = "dmz-tcp-80"
  priority                    = 100
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "tcp"
  source_port_range           = "*"
  destination_port_range      = "80"
  source_address_prefix       = "*"
  destination_address_prefix  = azurerm_virtual_network.dmz.address_space[0]
  resource_group_name         = var.resource-group-name
  network_security_group_name = azurerm_network_security_group.dmz-inbound.name
}

resource "azurerm_network_security_rule" "dmz-tcp-443" {
  name                        = "dmz-tcp-443"
  priority                    = 101
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "tcp"
  source_port_range           = "*"
  destination_port_range      = "443"
  source_address_prefix       = "*"
  destination_address_prefix  = azurerm_virtual_network.dmz.address_space[0]
  resource_group_name         = var.resource-group-name
  network_security_group_name = azurerm_network_security_group.dmz-inbound.name
}

resource "azurerm_network_security_rule" "dmz-deny" {
  name                        = "dmz-deny"
  priority                    = 900
  direction                   = "Inbound"
  access                      = "Deny"
  protocol                    = "tcp"
  source_port_range           = "*"
  destination_port_range      = "*"
  source_address_prefix       = "0.0.0.0/32"
  destination_address_prefix  = azurerm_virtual_network.dmz.address_space[0]
  resource_group_name         = var.resource-group-name
  network_security_group_name = azurerm_network_security_group.dmz-inbound.name
}
