resource "azurerm_network_security_group" "restricted" {
  name                = "restricted-access"
  location            = var.location
  resource_group_name = var.resource-group-name
}

resource "azurerm_network_security_group" "dmz-inbound" {
  name                = "dmz-access"
  location            = var.location
  resource_group_name = var.resource-group-name
}

resource "azurerm_network_security_group" "bastion" {
  name                = "bastion-access"
  location            = var.location
  resource_group_name = var.resource-group-name
}
