variable "network_details" {
  type        = map(string)
  description = "Standard set of networks for new environments"
}

variable "resource-group-name" {
  type        = string
  description = "The resource group name"
}

variable "location" {
  type        = string
  description = "The location for the infrastructure"
}

variable "tags" {
  type = map
  default = {
    terraform = true
  }
}

locals {
  bastion = {
    network_prefix = list(var.network_details.bastion_network_prefix)
    tags = merge(var.tags, {
      subnetrole = "bastion"
    })
  }
  dmz = {
    network_prefix = list(var.network_details.dmz_network_prefix)
    tags = merge(var.tags, {
      subnetrole  = "dmz"
      trafficflow = "public-inbound"
    })
  }
  restricted = {
    network_prefix = list(var.network_details.restricted_network_prefix)
    tags = merge(var.tags, {
      subnetrole     = "restricted"
      trafficflow    = "internal"
      restrictedType = "bastion-only"
    })
  }
}


