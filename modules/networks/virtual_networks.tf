# locals {
#   bastion = {
#     network_prefix = var.network_details.bastion_network_prefix
#     tags           = ["bastion"]
#   }
#   dmz = {
#     network_prefix = var.network_details.dmz_network_prefix
#     tags           = ["dmz", "public-inbound"]
#   }
#   restricted = {
#     network_prefix = var.network_details.restricted_network_prefix
#     tags           = ["restricted", "internal", "bastion-only"]
#   }

# }

resource "azurerm_virtual_network" "restricted" {
  name                = "restricted"
  address_space       = local.restricted.network_prefix
  location            = var.location
  resource_group_name = var.resource-group-name
  tags                = local.restricted.tags
}

resource "azurerm_virtual_network" "dmz" {
  name                = "dmz"
  address_space       = local.dmz.network_prefix
  location            = var.location
  resource_group_name = var.resource-group-name
  tags                = local.dmz.tags
}

resource "azurerm_virtual_network" "bastion" {
  name                = "bastion"
  address_space       = local.bastion.network_prefix
  location            = var.location
  resource_group_name = var.resource-group-name
  tags                = local.bastion.tags
}

