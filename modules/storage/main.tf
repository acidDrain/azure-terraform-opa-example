module "emptyDisk" {
  # required - change to filespec if testing locally, e.g. "./terraform-azurerm-manageddisk"
  source = "Azure/manageddisk/azurerm"

  # optional - but unless overridden all disks will have the same name
  managed_disk_name = "myEmptyManagedDisk"

  # required
  resource_group_name = "${azurerm_resource_group.diskRg.name}"

  # required for a new empty disk
  disk_size_gb = 1

  # optional - defaults to location of resource group if not provided
  location = "west us"

  # optional - defaults to Premium_LRS. These are the only two options.
  storage_account_type = "Standard_LRS"
}

output "empty_disk_id" {
  description = "The id of the newly created managed disk"
  value       = "${module.emptyDisk.managed_disk_id}"
}
data "azurerm_client_config" "current" {}

resource "azurerm_key_vault" "server-storage-encryption" {
  name                = "linux-storage-des-kv"
  location            = azurerm_resource_group.main.location
  resource_group_name = azurerm_resource_group.main.name
  tenant_id           = data.azurerm_client_config.current.tenant_id
  sku_name            = "premium"

  access_policy {
    tenant_id = data.azurerm_client_config.current.tenant_id
    object_id = data.azurerm_client_config.current.object_id

    key_permissions = [
      "create",
      "get",
      "delete",
      "list",
      "wrapkey",
      "unwrapkey",
      "get",
    ]

    secret_permissions = [
      "get",
      "delete",
      "set",
    ]
  }
}

resource "azurerm_key_vault_key" "linux-storage-key" {
  name         = "linux-storage-des"
  key_vault_id = azurerm_key_vault.server-storage-encryption.id
  key_type     = "RSA"
  key_size     = 2048

  key_opts = [
    "decrypt",
    "encrypt",
    "sign",
    "unwrapKey",
    "verify",
    "wrapKey",
  ]
}

resource "azurerm_disk_encryption_set" "linux-storage-encryption" {
  name                = "des"
  resource_group_name = azurerm_resource_group.main.name
  location            = azurerm_resource_group.main.location
  key_vault_key_id    = azurerm_key_vault_key.linux-storage-key.id
  identity {
    type = "SystemAssigned"
  }
}
