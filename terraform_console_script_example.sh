#!/bin/sh

set -e

if [ -e ./.env ]; then
  . ./.env
fi

terraform init -reconfigure -backend-config="token=${TF_CLOUD_TOKEN}"

echo "var.network_details.bastion_network_prefix" | terraform console -var 'network_details={ restricted_network_prefix = "192.168.2.0/24", bastion_network_prefix = "192.168.3.0/24", dmz_network_prefix = "192.168.4.0/24" }'

# echo "var.network_details.bastion_network_prefix" | terraform console -var-file ./defs.auto.tfvars
