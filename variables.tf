variable "ARM_CLIENT_ID" {}

variable "ARM_CLIENT_SECRET" {}

variable "ARM_SUBSCRIPTION_ID" {}

variable "ARM_TENANT_ID" {}

variable "target" {
  type        = map(any)
  default     = { location = "Central US", productname = "azure-terraform-opa-demo" }
  description = "The product family and location the infrastructure will support"
}

variable "sysadmin-pub-rsa-key" {
  type        = string
  description = "SSH Key to authenticate sysadmin user Linux systems"
}

variable "sysadmin" {
  type        = string
  default     = "sysadmin"
  description = "Administrative user for Linux systems"
}

variable "servername" {
  type        = string
  default     = "ubuntuserver"
  description = "The name of the server"
}

variable "network_details" {
  type        = map(string)
  default     = { restricted_network_prefix = "192.168.2.0/24", bastion_network_prefix = "192.168.3.0/24", dmz_network_prefix = "192.168.4.0/24" }
  description = "A list of objects that describe network resources that should be created. Identifier is a friendly name for the network, subnet prefix should be a network address/prefix-length such as 192.168.1.0/24. Protected should be true or false, which signifies the network should be isolated via bastion access."
}

